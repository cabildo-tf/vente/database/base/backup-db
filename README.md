# Backup DB
Backup DB is a docker for create database backup (PostgreSQL)
and copy it to backup folder. Also sends metrics a Prometheus through
PushGateWay.

![alt schema-db](images/schema-backup-db.png)

# Variables
|          Name         	|          Description          	|      Default      	|
|---------------------------|-----------------------------------|----------------------	|
| POSTGRES_USER         	| Database username             	| postgres          	|
| POSTGRES_PASSWORD     	| Database password             	| password          	|
| POSTGRES_HOSTNAME     	| Database hostname             	| postgresql        	|
| POSTGRES_DB           	| Database name                 	| postgres           	|
| PUSHGATEWAY_HOST      	| PushGateWay hostname          	| pushgateway:9091  	|
| PUSHGATEWAY_JOB       	| PushGateWay job name          	| POSTGRES_HOSTNAME 	|
| MINIO_HOSTNAME            | Minio hostname                    |                       |
| MINIO_ARGS                | mc arguments: --insecure, etc.    |                       |
| MINIO_BUCKET              | Bucket upload backup              |                       |
| ACCESS_KEY                | Minio access key                  |                       |
| SECRET_KEY                | Minio secret key                  |                       |


# Metrics
|                           Name                          |                        Description                       	   |
|---------------------------------------------------------|----------------------------------------------------------------|
| backup_db{label="vente"}                                | Outcome of the backup database job (0=failed, 1=success) 	   |
| backup_duration_seconds{label="vente"}                  | Duration of create backup execution in seconds             	   |
| backup_size_bytes{label="vente"}                        | Duration of the script execution in seconds              	   |
| backup_created_date_seconds{label="vente"}              | Created date in seconds                                  	   |


# Conexión Minio

Conectarse al minio.

```bash
mc alias set minio https://minio.vente.tenerife.int
```

Creación del usuario.

```bash
mc admin user add minio vente-db-backup-user
mc admin group add minio vente-db-backup-group vente-db-backup-user
```

Crear un fichero vente-db-backup-policy.json

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Resource": [
                "arn:aws:s3:::vente-db-backups"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:PutObject",
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::vente-db-backups/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": [
                "arn:aws:s3:::*"
            ]
        }
    ]
}

```

Crear política de acceso al bucket.

```bash
mc admin policy add minio vente-db-backup-policy vente-db-backup-policy.json
```

Se aplican las políticas al grupo.

```bash
mc admin policy set minio vente-db-backup-policy group=vente-db-backup-group --insecure
```

Se crea el bucket.

```bash
mc mb minio/vente-db-backups --insecure
```

Aplica política de retención de backups 2 días.

```bash
mc ilm import minio/vente-db-backups --insecure <<EOF
{
  "Rules": [
   {
    "Expiration": {
     "Days": 2
    },
    "ID": "deleteOldBackups",
    "Status": "Enabled"
   }
  ]
}
EOF
```