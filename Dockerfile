FROM alpine:3.14

LABEL maintainer="ilorgar@gesplan.es"

ENV POSTGRES_PORT="5432" \
	POSTGRES_HOSTNAME="postgresql" \
	POSTGRES_USER="postgres" \
	POSTGRES_PASSWORD="password" \
	POSTGRES_DB="postgres" \
	POSTGRES_PASS_FILE='/root/.pgpass'

ARG POSTGRES_CLIENT_VERSION="11"

COPY rootfs /

RUN apk add --no-cache \
		postgresql-client=~${POSTGRES_CLIENT_VERSION} \
			--repository=http://dl-cdn.alpinelinux.org/alpine/v3.10/main && \
	apk add --no-cache \
		curl=~7 \
		bash=~5 \
		coreutils=~8 \
		grep=~3

ENTRYPOINT ["/bin/bash", "-c", "backup"]